import Document, { Html, Head, Main, NextScript } from 'next/document'
import flush from 'styled-jsx/server'

const businessPrefix = 'og: http://ogp.me/ns# fb: http://ogp.me/ns/fb# business: http://ogp.me/ns/business#';
const articlePrefix = 'og: http://ogp.me/ns# fb: http://ogp.me/ns/fb# article: http://ogp.me/ns/article#';

export default class MyDocument extends Document {
  static getInitialProps ({ pathname, renderPage }) {
    const {html, head, errorHtml, chunks} = renderPage()
    const styles = flush()
    return { pathname, html, head, errorHtml, chunks, styles }
  }

  render () {
    return (
     <Html>
       <Head prefix={this.props.pathname === '/' ? businessPrefix : articlePrefix}/>
       <body>
         <Main />
         <NextScript />
       </body>
     </Html>
    )
  }
}