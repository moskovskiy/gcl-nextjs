import Head from 'next/head'
import Image from 'next/image'

import { useTranslation, Trans } from "react-i18next"
import { useState } from 'react'
import axios from 'axios'

import Header from '../components/header/header'
import Heading from '../components/heading/heading'
import View from '../components/view/view'
import Input from '../components/input/input'
import Divider from '../components/divider/divider'
import Text from '../components/text/text'
import Isle from '../components/isle/isle'
import Hint from '../components/hint/hint'
import Social from '../components/social/social'
import Footer from '../components/footer/footer'

export default function Home() {
  const { t, i18n } = useTranslation();

  const changeLanguage = lng => {
    i18n.changeLanguage(lng);
  };

  let [wrong, setWrong] = useState(false)
  let [link, setLink] = useState("")
  let [hint, setHint] = useState(t('👋  / Ссылка из Clubhouse'))
  let [btnText, setBtn] = useState("")

  function updateLink (e) {
    setLink(e.target.value)
    setWrong(false)
    if(e.target.value != "") {
        setBtn(t('Улучшить'))
    } else {
        setBtn("")
        //setBtn(t('Вставить'))
    }
  }

  function generateLink() {
    if(link != "") {
        axios.post('https://api.getclub.link/api/v1/cover', {
            link: link,
            type:"to"
        }).then((response) => {
            localStorage.setItem('created', "true")
            window.location.href = response.data.covered
        })
        .catch(function (error) {
            setWrong(true)
            //alert(JSON.stringify(error))
        })
    } else {
        let req = async (dispatch) => {
            const text = await navigator.clipboard.readText()
            link = text
            setHint(text)
        }
        req()
    }
  }

  return (
    <div className="container">
      <Head>
        <meta charset="utf-8" />
        <link rel="icon" href="%PUBLIC_URL%/favicon.ico" />
        <meta name="viewport" content="width=device-width, initial-scale=1" />
        <meta name="theme-color" content="#000000" />

        <meta
          name="description"
          lang="ru"
          content="Улучшатель ссылок для Clubhouse GetClub.link"
        />

        <meta
          name="description"
          lang="en"
          content="Clubhouse event links beautifier GetClub.link"
        />

        <link rel="manifest" href="%PUBLIC_URL%/manifest.json" />

        <title>GetClub.link</title>
        <title lang="en">GetClub.link: Clubhouse links beautifier</title>
        <title lang="ru">GetClub.link: Улучшатель ссылок для Clubhouse</title>

        <link rel="shortcut icon" href="/favicon.ico" />
        <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1"/>
      </Head>

      {/*------------------------------*/}
      <main>
        <Header/>
        <View>
          <Heading maxi={true}>{t('Правильные ссылки для вашей комнаты в Clubhouse')}</Heading>
          <Text>{t('Создавайте ссылки, которыми круто делиться в соцсетях!')}</Text>
          <Input type="submit" onKeyDown={() => onKeyDownHandler()} value={link} onChange={updateLink} autoFocus={true} hint={hint} action={btnText}
            onClick={() => generateLink()}
          />
          
          {wrong && <>
              <Divider/>
              <Hint>{t('Некорректная ссылка на событие в Clubhouse')}</Hint>
          </>}
          <Divider/>

          <Divider/>
          <Divider/>

          <Heading>{t('Получайте больше переходов с информативными превью')}</Heading>
          <Text>{t('Описание, спикеры и даты мероприятия отображаются на предпросмотре в соцсетях, вовлекая аудиторию')}</Text>
          <Divider/>


          <Isle>
            <Hint>{t('Обычная ссылка 🤔️')}</Hint>
            <Image src={'/old.png'} width="300px" height="160px" className="Image"></Image>
            <Divider/>
          </Isle>

          <Isle>
              <Hint>{t('Что вы получите! 😎 ')}</Hint>
              <Image src={'/new.png'} width="300px" height="160px" className="Image"></Image>
          </Isle>

          <Divider/>
          <Heading>{t('Приятно делиться в соц. сетях')}</Heading>
          <Text>{t('Никакого копипаста, удобные ссылки, чтобы поделиться')}</Text>
          <Social name ="по ссылке GetClub.link!" link='https://getclub.link/'/>
          <Divider/>
          <Text>
              {t('+ Отображается название мероприятия')}<br/>
              {t('+ Отображается дата и время события')}<br/>
              {t('+ Отображаются ваши спикеры с фото')}<br/>
              {t('+ Отображаем события в правильном часовом поясе')}
          </Text>

          <Divider/>
          <Divider/>
          <Divider/>
          <Heading>{t('И многое другое в скором времени...')}</Heading>
        </View>
        <Footer/>
      </main>
      {/*------------------------------*/}


      <style jsx>{`
      `}</style>

      <style jsx global>{`
        @import url('https://fonts.googleapis.com/css2?family=Nunito:ital,wght@0,400;0,600;0,700;0,800;0,900;1,400;1,600&display=swap');

        * {
          --color-bg: #f2efe4;
          --color-dark: #1F2024;
          --color-accent: #2A653F;
          --color-tint: #A08D8D;
          --color-light: #898989;
        }
        
        body {
          margin: 0;
          font-family: 'Nunito', -apple-system, BlinkMacSystemFont, 'Segoe UI', 'Roboto', 'Oxygen',
            'Ubuntu', 'Cantarell', 'Fira Sans', 'Droid Sans', 'Helvetica Neue',
            sans-serif;
          -webkit-font-smoothing: antialiased;
          -moz-osx-font-smoothing: grayscale;
          background-color: var(--color-bg);
          color: var(--color-dark);
          overflow-x: hidden;
        }
        
        a:link {
          text-decoration: none;
        }
      `}</style>
    </div>
  )
}
