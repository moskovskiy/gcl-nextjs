import React, { useState, useEffect } from 'react'
import Head from 'next/head'
import Header from '../../components/header/header'
import Heading from '../../components/heading/heading'
import View from '../../components/view/view'
import Button from '../../components/button/button'
import Input from '../../components/input/input'
import Divider from '../../components/divider/divider'
import Download from '../../components/download/download'
import House from '../../components/house/house'
import Hint from '../../components/hint/hint'
import Social from '../../components/social/social'
import Footer from '../../components/footer/footer'
import { useTranslation, Trans } from "react-i18next"
import { useRouter } from 'next/router'
import axios from 'axios'
import { Helmet } from 'react-helmet'

function timify (ts) {
    return new Date(ts * 1000).toUTCString().substr(0, 22)
}

function tryToGetParamsFromBrowser() {
    // make sure we don't crash on server side
    if (typeof window === 'undefined') {
      return null;
    }
  
    const path = window.location.pathname;
  
    const paramsInEndpoint = [];
    const endpointMatcher = endpoint
      // find each [foo] part and extract 'foo' from it.
      .replace(/\[([a-z]+)\]/gi, (_, paramName) => {
        // push it to list of params in order they appear
        paramsInEndpoint.push(paramName);
  
        // replace [foo] with regexp part that will match actual values
        return '([a-zA-Z0-9-]+)';
      })
      // escpae '/' parts of endpoint so we can use it in regexp
      .replace(/\//g, '\\/');
  
    // create regexp that will pick actual values from window path
    const endpointRegexp = new RegExp(endpointMatcher, 'ig');
  
    // let's try to match it
    const result = endpointRegexp.exec(path);
  
    // if current browser path is not matching - give up
    if (!result) {
      return null;
    }
  
    // we will have actual values as array here in the same order as param names in paramsInEndpoint
    const [, ...routeParamsValues] = result;
  
    // let's create a map of params
    const paramsFromPath = {};
  
    // now, let's connect param names and values and add results to our map
    routeParamsValues.forEach((value, index) => {
      // index in array of values is the same as index of param name
      const paramName = paramsInEndpoint[index];
  
      // add it to params map
      paramsFromPath[paramName] = value;
    });
  
    return paramsFromPath;
}

Home.getInitialProps = async function(ctx) {
    const res = await axios.get('https://api.getclub.link/api/v1/info/' + ctx.query.type+"/"+ctx.query.id)
    const data = await res.data

    return {
        event: data,
        wrong: true,
        path: ctx.query.type+"/"+ctx.query.id,
        request: 'https://api.getclub.link/api/v1/info/' + ctx.query.type+"/"+ctx.query.id
    }
}

export default function Home({event, wrong, path, request}) {
    //console.log (JSON.stringify(request) + " " + JSON.stringify(event) + " " + JSON.stringify(wrong))
    const router = useRouter()
    const { t, i18n } = useTranslation()
    //let [wrong, setWrong] = useState("")
    let [success, setSuccess] = useState(false)

    //console.log (JSON.stringify(event))
    
    let rt = router.query

    if (typeof router.query === 'undefined') {
        rt = tryToGetParamsFromBrowser()
    }

    let link = rt.type + '/' + rt.id /*router.pathname.replace(/^(?:\/\/|[^/]+)*\//, '')

    if (link == '') {
        link = window.location.href.replace(/^(?:\/\/|[^/]+)*\//, '')
    }*/

    //let link = path;


    useEffect(() => {
        if (localStorage.getItem('created') == "true") {
            localStorage.setItem('created', "")
            setSuccess(true)
        }
    })

    return (
        <div className="container">
        <Head>
            <title>{(event.data)? (event.data.title + " – ") : ""} GetClub.link</title>
            <title lang="en">{(event.data)?("Clubhouse event: " + event.data.title + " – GetClub.link"):" GetClub.link"}</title>
            <title lang="ru">{(event.data)?("Событие в Clubhouse: " + event.data.title + " – GetClub.link"):" GetClub.link"}</title>
            <meta name="description" content={timify(event.data.dtstart) + ": " + event.data.description} />
            <meta name="image" content={"https://api.getclub.link/preview/" + link} />
            <meta id="og-title" property="og:title" content={(event.data)? (event.data.title + " – GetClub.link"):" GetClub.link"}/>
            <meta id="og-image" property="og:image" content={"https://api.getclub.link/preview/" + link} />
            <meta property="og:description" content={(event.data)? (timify(event.data.dtstart) + ": " + event.data.description):" GetClub.link"}/>
            <meta name="twitter:site" content="@getclub_link"/>
            <meta name="twitter:card" content="summary_large_image" />
            <meta name="twitter:title" content={(event.data)? (event.data.title + " – GetClub.link"):" GetClub.link"} />
            <meta name="twitter:description" content={(event.data)? (timify(event.data.dtstart) + ": " + event.data.description):" GetClub.link"} />
            <meta name="twitter:url" content={"https://getclub.link/"+link} />
            <meta name="twitter:image" content={"https://api.getclub.link/preview/" + link} />
            <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1"/>
            <link rel="shortcut icon" href="/favicon.ico" />
        </Head>
        <main>
        <Header/>
        <>{(wrong && event.data) ? <View>
            {success && <>
                <Heading>🖖 {t('Ваша ссылка готова!')}</Heading>
                <Input value={window.location.href} action={t('📋 ')} onClick={()=>navigator.clipboard.writeText(window.location.href)}></Input>
                <Divider/>
                <Hint>{t('Поделиться')}</Hint>
                <Social name={event.data.title} link={"https://getclub.link/" + path} selling={false}/>
                <Divider/>
                </>
            }
            {event.data.pics && <House
                avatars={event.data.pics}
                time={timify(event.data.dtstart)}
                header={event.data.title}
                from="GOOD TIME"
                members={event.data.guests}
                description={event.data.description}
            />}
            <a href={"https://joinclubhouse.com/event/"+event.data.hash}><Button>{t('Открыть в приложении')}</Button></a>
            {!success && <>
                <Divider/>
                <Hint>{t('Поделиться')}</Hint>
                <Social name={event.data.title} link={"https://getclub.link/" + path} selling={false}/>
            </>}
            <Download/>
            <Divider/>

            {/*<MetaTags>
                <title>Page 1</title>
                <meta id="meta-description" lang="en" content={"Clubhouse event: " + event.description} />
                <meta id="meta-description" lang="ru" content={"Событие в Clubhouse: " + event.description} />
                <meta id="og-title" property="og:title" content={t('Событие в Clubhouse') + " " + event.title}/>
                <meta id="og-image" property="og:image" content={"https://api.getclub.link/https://api.getclub.link/preview/to/"+link} />
            </MetaTags>*/}

        </View> : <View>
                <House
                avatars={[]}
                time={"..."}
                header={"..."}
                from="..."
                members={"..."}
                description={"..."}/>
                <Social selling={true}/>
                <Download/>
            <View/>
        </View>}</>
        <Footer/>
        </main>

        <style jsx>{`
        `}</style>

        <style jsx global>{`
        @import url('https://fonts.googleapis.com/css2?family=Nunito:ital,wght@0,400;0,600;0,700;0,800;0,900;1,400;1,600&display=swap');

            * {
            --color-bg: #f2efe4;
            --color-dark: #1F2024;
            --color-accent: #2A653F;
            --color-tint: #A08D8D;
            --color-light: #898989;
            }

            body {
            margin: 0;
            font-family: 'Nunito', -apple-system, BlinkMacSystemFont, 'Segoe UI', 'Roboto', 'Oxygen',
                'Ubuntu', 'Cantarell', 'Fira Sans', 'Droid Sans', 'Helvetica Neue',
                sans-serif;
            -webkit-font-smoothing: antialiased;
            -moz-osx-font-smoothing: grayscale;
            background-color: var(--color-bg);
            color: var(--color-dark);
            overflow-x: hidden;
            }

            a:link {
            text-decoration: none;
            }
        `}</style>
        </div>
    )
}
