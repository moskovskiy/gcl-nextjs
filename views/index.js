import React from 'react'

import {
    BrowserRouter as Router,
    Switch,
    Route,
    Link
} from "react-router-dom"

import axios from 'axios'
import NotFound from './404'
import Landing from './landing'
import Room from './room'
import Success from './success'

import Header from '../components/header/header'
import Footer from '../components/footer/footer'

function App() {
  return (
    <div className="App">
        <Header/>
        <Router>
            <Switch>   
                <Route exact path="/" component={Landing}/>
                <Route exact path="/event/:id" component={Room}/>
                <Route exact path="/to/:id" component={Room}/>
                <Route exact path="/created/:id" component={Success}/>
                <Route component={NotFound}/>
            </Switch>
        </Router>
        <Footer/>
    </div>
  );
}

export default App;
