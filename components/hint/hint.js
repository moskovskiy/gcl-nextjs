import React from 'react';

function Hint(props) {
  return (
    <div className="Hint">
        {props.children}

        <style jsx>{`
        .Hint {
          margin-top: 15px;
          font-size: 20px;
          font-weight: 700;
          color: var(--color-tint);
          margin-bottom: 15px;
          margin-left: 10px;
          margin-right: 10px;
        }
        `}</style>
    </div>
  );
}

export default Hint;
