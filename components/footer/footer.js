import React from 'react';
import Image from 'next/image'

import Text from '../text/text'
import Hint from '../hint/hint'
import Heading from '../heading/heading'
import { useTranslation, Trans } from "react-i18next"


function Footer() {
    const { t, i18n } = useTranslation();

    return (
        <div className="Footer">
            <style jsx>{`
                .Footer {
                    margin-top: 100px;
                    text-align: center;
                    width: 100vw;
                    height: 50px;
                    margin-bottom: 100px;
                }
                
                .Footer__ad {
                    width: 150px;
                    display: inline-block;
                    vertical-align: top;
                    margin-left: 50px;
                    margin-right: 50px;
                    margin-bottom: 30px;
                    text-align: center;
                }
                
                .Footer__ad:hover {
                    opacity: 0.5;
                }
            `}</style>
            <Heading>🖖 </Heading>
            <Hint>{t('Создайте удобные ссылки на мероприятие в Clubhouse в один клик')}</Hint>
            <Text>{t('Связаться с нами')}</Text>
            <div style={{marginTop: 20}}/>
            <br/>
            <a href="https://yadda.io" target="_blank"><Image width="90px" height="30px" src="/link.png" className="Image Footer__ad"/></a>
            <br/><br/>
            <a href="https://moskovskiy.org" target="_blank"><Image width="120px" height="37px" src="/link3.png"  className="Image Footer__ad"/></a>
            <br/><br/>
        </div>
    );
}

export default Footer;
