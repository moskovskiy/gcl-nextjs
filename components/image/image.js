import React from 'react';
import image from 'next/image'

function Image(props) {
  return (
    <image src={props.src} className="Image">
    </image>
  );
}

export default Image;
