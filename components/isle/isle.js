import React from 'react'

function Isle (props) {
  return (
    <span className="Isle">
        {props.children}

        <style jsx>{`
        .Isle {
          width: 290px;
          text-align: center;
          display: inline-block;
          vertical-align: top;
          margin-right: 10px;
        }
        
        @media screen and (min-width: 1000px) {
            .Isle {
                width: 450px;
                margin-right: 25px;
            }
        }
        `}</style>
    </span>
  );
}

export default Isle;
