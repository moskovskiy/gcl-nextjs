import React from 'react';

function Divider() {
  return (
    <div className="Divider">
    <style jsx>{`
      .Divider {
        display:block;
        width: 100%;
        height: 50px;
      }
    `}</style>
    </div>
  );
}

export default Divider;
