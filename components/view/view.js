import React from 'react'

/*

*/

function View(props) {
    
    if (props.title) {
        document.title = props.title + " – GetClub.link"
    }

    //document.getElementsByTagName('meta')["description"].content = props.description

    return <div className="UIView">
            {props.children}
            
            <style jsx>{`
                .UIView {
                    margin-top: 80px;
                    width: calc(100vw - 40px);
                    margin-left: 20px;
                    margin-right: 20px;
                    text-align: center;
                    align-content: center;
                    overflow-x: hidden;
                }
                
                @media screen and (min-width: 600px) {
                    .UIView {
                        margin-top: 80px;
                        width: 600px;
                        margin-left: calc(50% - 300px);
                    }
                }
                
                @media screen and (min-width: 1000px) {
                    .UIView {
                        margin-top: 90px;
                        width: 1000px;
                        margin-left: calc(50% - 500px);
                    }
                }
            `}</style>
    </div>
}

export default View;
