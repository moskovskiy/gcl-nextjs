import React from 'react';
import Image from 'next/image'

import { useTranslation, Trans } from "react-i18next";

function Header() {
    const { t, i18n } = useTranslation();
    
    const changeLanguage = lng => {
      i18n.changeLanguage(lng);
    };
    return (
        <div className="Header">
            <div className="Header__left">
                <a href="/">
                    <Image width='40px' height='40px' src={'/logo.png'} className="Header__logo"/>
                    <div className="Header__name">GetClub.link</div>
                </a>
            </div>

            <div className="Header__right">
                <a href="/"><div className="Header__link">＋</div></a>
            </div>

            <style jsx>{`
                .Header {
                    top: 0px;
                    left: 0px;
                    width: 100vw;
                    height: 40px;
                    padding-top: 20px;
                    padding-bottom: 20px;
                }
                
                .Header__left {
                    margin-left: 20px;
                }
                
                .Header__right {
                    vertical-align: top;
                    float: right;
                    margin-right: 20px
                }
                
                .Header__link {
                    display: block;
                    color: var(--color-tint) !important;
                    font-size: 40px;
                    font-weight: 100;
                    margin-top: -56px;
                }
                
                .Header__logo {
                    display: inline-block;
                    vertical-align: top;
                    width: 26px;
                    pointer-events: none;
                    color: var(--color-dark);
                }
                
                .Header__name {
                    display: inline-block;
                    vertical-align: top;
                    margin-top: 5px;
                    margin-left: 10px;
                    font-size: 16px;
                    font-weight: 800;
                    color: var(--color-dark);
                }
                
                .Header__name:hover {
                    color: var(--color-tint);
                }
                
                @media screen and (min-width: 600px) {
                    .Header {
                        height: 80px;
                    }
                
                    .Header__left {
                        margin-left: 40px;
                    }
                
                    .Header__right {
                        margin-right: 40px;
                    }
                
                    .Header__link {
                        font-size: 40px;
                        font-weight: 100;
                    }
                
                    .Header__logo {
                        width: 40px;
                    }
                
                    .Header__name {
                        margin-top: 10px;
                        font-size: 23px;
                    }
                }
            `}</style>
        </div>
    );
}

export default Header;
