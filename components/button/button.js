import React from 'react';

function Button(props) {
  return (
    <button className="Button">
      {props.children}
      <style jsx>{`
      .Button {
          margin-top: 20px;
          background: none;
          box-shadow: none;
          background-color: var(--color-accent);
          color: white;
          width: 100%;
          max-width: 400px;
          font-family: 'Nunito';
          font-size: 16px;
          font-weight: 700;
          height: 50px;
          border-radius: 22px;
          border: none;
          margin-bottom: 20px;
          cursor: pointer;
      }
      `}</style>
    </button>
  );
}

export default Button;
