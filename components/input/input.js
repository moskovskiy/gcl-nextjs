import React from 'react'

export default function Input(props) {
    function checkEnter (e) {
        if (e.key === 'Enter') {
            props.onClick()
        }
    }

    return (<>
        <div className="Input">
            <input onKeyDown={checkEnter} tabIndex="0" autoFocus={props.autoFocus} pattern={props.pattern} placeholder={props.hint} value={props.value} onChange={props.onChange} className={(props.action != "")? "Input__field" : "Input__field Input__field--wide"}/>
    {(props.action != "") && <button onClick={props.onClick} className="Input__action">{props.action}</button>}
        </div>

        <style jsx>{`
            .Input {
                margin-top: 20px;
                background-color: white;
                margin-left: 10px;
                margin-right: 10px;
                width: calc(100% - 20px);
                box-shadow: 0px 2px 20px 6px #2c2c2e1c;
                border-radius: 10px;
                height: 70px;
                text-align: left;
            }
            
            .Input__field {
                padding-top: 10px;
                display: block;
                height: 49px;
                border: none;
                background: none;
                color: var(--color-dark);
                padding-left: 20px;
                outline: none;
                font-family: 'Nunito';
                font-size: 16px;
                font-weight: 700;
                z-index: 2;
                width: calc(100% - 115px);
            }
            
            .Input__field--wide {
                width: calc(100% - 15px);
            }
            
            .Input__action {
                display: block;
                float: right;
                margin-top: -50px;
                height: 49px;
                z-index: 1;
                background: none;
                font-family: 'Nunito';
                font-size: 14px;
                font-weight: 700;
                width: 100px;
                color: var(--color-accent);
                text-align: center;
                box-shadow: none;
                border: none;
                outline: none;
                cursor: pointer;
            }
            
            .Input__action:hover {
                color: var(--color-light);
            }
            
            
            @media screen and (min-width: 600px) {
            
                .Input {
                    margin-top: 40px;
                    background-color: white;
                    width: calc(100% - 20px);
                    border-radius: 20px;
                    padding-bottom: 10px;
                    padding-top: 10px;
                    max-width: 600px;
                }
            
                .Input__field {
                    display: block;
                    height: 49px;
                    border: none;
                    background: none;
                    color: var(--color-dark);
                    padding-left: 25px;
                    outline: none;
                    font-family: 'Nunito';
                    font-size: 26px;
                    font-weight: 700;
                    width: 100%;
                    z-index: 2;
                    width: calc(100% - 140px);
                }
            
                .Input__field--wide {
                    width: calc(100% - 15px);
                }
            
                .Input__action {
                    display: block;
                    float: right;
                    margin-top: -50px;
                    height: 49px;
                    z-index: 1;
                    background: none;
                    font-family: 'Nunito';
                    font-size: 18px;
                    font-weight: 700;
                    width: 120px;
                    color: var(--color-accent);
                    text-align: center;
                    box-shadow: none;
                    border: none;
                    outline: none;
                    margin-right: 10px;
                    text-align: right;
                    padding-right: 20px;
                }
            
                .Input__action:hover {
                    color: var(--color-light);
                }
            
            }
            
            @media screen and (min-width: 1000px) {
                .Input {
                    margin-left: 200px;
                    width: 600px;
                }
            }
        `}</style>
    </>)
}
  