import React from 'react'

function Text(props) {
  return (
    <div className="Text">
        {props.children}

        <style jsx>{`
        .Text {
            margin-top: 30px;
            font-size: 17px;
            font-weight: 600;
        }
        `}</style>
    </div>
  );
}

export default Text;
